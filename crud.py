from model import Survey, Response, SurveyGroup, db
from werkzeug.security import generate_password_hash, check_password_hash

def create_response(comment, x_pos, y_pos, color, name, survey):
    response = Response(
        comment=comment,
        x_pos=x_pos,
        y_pos=y_pos,
        color=color,
        respondent_name=name,
        survey=survey
    )
    db.session.add(response)
    db.session.commit()

def create_survey_group(code, password_hash):
    sg = SurveyGroup(
        group_code=code,
        password_hash=password_hash,
    )
    db.session.add(sg)
    db.session.commit()
    return sg

def create_survey(xneg, xpos, yneg, ypos, ask_names, auth_code="", private=False, survey_group=None):
    new_survey = Survey(
        x_pos_label=xpos, 
        y_pos_label=ypos,
        x_neg_label=xneg,
        y_neg_label=yneg,
        ask_names=ask_names,
        survey_group=survey_group,
        auth_code=auth_code
    )
    db.session.add(new_survey)
    db.session.commit()
    return new_survey


def create_survey_from_form(form_response):
    xneg = form_response['xneg']
    xpos = form_response['xpos']
    yneg = form_response['yneg']
    ypos = form_response['ypos']
    ask_names = 'ask_name' in form_response

    new_survey  = Survey(
        x_pos_label=xpos, 
        y_pos_label=ypos,
        x_neg_label=xneg,
        y_neg_label=yneg,
        ask_names=ask_names)

    db.session.add(new_survey)
    db.session.commit()
    return new_survey.survey_id

def get_survey(id):
    return Survey.query.get(id)