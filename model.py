from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()

class SurveyGroup(db.Model):
    __tablename__ = 'survey_groups'

    group_code = db.Column(db.String(30), primary_key=True)
    password_hash = db.Column(db.String(150))
    time_created = db.Column(db.DateTime, nullable=False, default=db.func.now())

    surveys = db.relationship('Survey', back_populates='survey_group')


class Survey(db.Model):
    __tablename__ = 'surveys'

    survey_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    x_pos_label = db.Column(db.String(30), nullable=False)
    x_neg_label = db.Column(db.String(30), nullable=False)
    y_pos_label = db.Column(db.String(30), nullable=False)
    y_neg_label = db.Column(db.String(30), nullable=False)
    ask_names = db.Column(db.Boolean, nullable=False)
    time_created = db.Column(db.DateTime, nullable=False, default=db.func.now())
    duration_hrs = db.Column(db.Integer)
    group_code = db.Column(db.String(30), db.ForeignKey(SurveyGroup.group_code))
    auth_code = db.Column(db.String(30))

    responses = db.relationship('Response', back_populates='survey')
    survey_group = db.relationship('SurveyGroup', back_populates='surveys')

    def as_dict(self):
        return {
            'code': self.survey_id,
            'xNegLabel': self.x_neg_label,
            'xPosLabel': self.x_pos_label,
            'yNegLabel': self.y_neg_label,
            'yPosLabel': self.y_pos_label,
            'askNames': self.ask_names,
            'responses': [r.as_dict() for r in self.responses],
            'groupCode': self.group_code,
            'timeCreated': self.time_created
        }

    def __repr__(self):
        return f'<Survey survey_id={self.survey_id} >'

class Response(db.Model):
    __tablename__ = 'responses'

    response_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    x_pos = db.Column(db.Integer, nullable=False)
    y_pos = db.Column(db.Integer, nullable=False)
    color = db.Column(db.String(30), nullable=False)
    comment = db.Column(db.Text)
    respondent_name = db.Column(db.Text)
    survey_id = db.Column(db.Integer, db.ForeignKey(Survey.survey_id))

    survey = db.relationship('Survey', back_populates='responses')

    def as_dict(self):
        return {
            'xPos': self.x_pos,
            'yPos': self.y_pos,
            'color': self.color,
            'comment': self.comment,
            'respondentName': self.respondent_name,
            'surveyID': self.survey_id
        }

    def __repr__(self):
        return f'<Response response_id={self.response_id} pos=({self.x_pos}, {self.y_pos} name={self.respondent_name})>'

def connect_to_db(app):
    with app.app_context():
        POSTGRES_HOST = os.environ["POSTGRES_HOST"]
        POSTGRES_USER = os.environ["POSTGRES_USER"]
        POSTGRES_PASSWORD = os.environ["POSTGRES_PASSWORD"]
        DB_NAME = os.environ.get("DB_NAME", "")
        app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{DB_NAME}'
        app.config['SQLALCHEMY_ECHO'] = False
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        db.app = app
        db.init_app(app)
        db.create_all()
        db.session.commit()
