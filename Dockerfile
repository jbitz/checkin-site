FROM python:3.10-alpine
WORKDIR /code
COPY requirements.txt /code/
RUN apk update && apk add python3-dev gcc libc-dev
RUN pip install -r requirements.txt
COPY . /code/