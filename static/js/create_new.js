async function onFormSubmit(e) {
    e.preventDefault();
    const data = {
        xneg: document.querySelector('#xneg').value,
        xpos: document.querySelector('#xpos').value,
        yneg: document.querySelector('#yneg').value,
        ypos: document.querySelector('#ypos').value,
        askName: document.querySelector('#ask_name').checked,
        isPrivate: document.querySelector('#visibility_private').checked,
        groupData: {
            code: document.querySelector('#group_code').value.toLowerCase().trim(),
            password: document.querySelector('#group_password').value,
        }
    }
    if (data.isPrivate && data.groupData.code === '') {
        document.querySelector('.error').innerText = 'You must enter a group name for your survey!';
        return;
    } else if (data.isPrivate && data.groupData.password == '') {
        document.querySelector('.error').innerText = 'You must enter a password for your group!';
        return;
    }

    let groupExists = true;
    if (data.groupData.code !== '') {
        groupExists = await fetch(makePath(`/api/groupexists/${data.groupData.code}`));
        groupExists = await groupExists.json();
        groupExists = groupExists.result;
        console.log(groupExists);
    }
    const msg = `Group ${data.groupData.code} does not exist yet! Click OK if you want to create a new one.`
    if (groupExists || confirm(msg)) {
        let createSurveyResult = await fetch(makePath('/api/createsurvey'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        });
        if (createSurveyResult.status == 403) {
            document.querySelector('.error').innerText = "Invalid credentials for group!";
            return;
        }
        createSurveyResult = await createSurveyResult.json();
        document.querySelector('.error').innerText = '';
        window.location.href = createSurveyResult.url;
    }
}

function setup_new() {
    const canvas = document.getElementById("main_canvas");
    const ctx = canvas.getContext("2d");

    const WIDTH = canvas.width;
    const HEIGHT = canvas.height;

    const xneg = document.getElementById('xneg');
    const xpos = document.getElementById('xpos');
    const yneg = document.getElementById('yneg');
    const ypos = document.getElementById('ypos');

    //Set up text event listeners
    function updateAxes() {
        graphData.xNegLabel = xneg.value;
        graphData.xPosLabel = xpos.value;
        graphData.yNegLabel = yneg.value;
        graphData.yPosLabel = ypos.value;
        redraw(ctx, WIDTH, HEIGHT);
    }
    updateAxes();

    xneg.addEventListener('input', updateAxes);
    xpos.addEventListener('input', updateAxes);
    yneg.addEventListener('input', updateAxes);
    ypos.addEventListener('input', updateAxes);

    const publicRadio = document.querySelector('#visibility_public');
    const privateRadio = document.querySelector('#visibility_private');
    const handleRadio = () => {
        document.querySelector('.warning').hidden = !publicRadio.checked;
        document.querySelector('.survey-group-options').hidden = publicRadio.checked;
    }
    publicRadio.addEventListener('change', handleRadio);
    publicRadio.checked = true;
    privateRadio.addEventListener('change', handleRadio);
    handleRadio();

    document.querySelector('#survey_form').addEventListener('submit', onFormSubmit);
    document.querySelector('#group_code').value = '';
    document.querySelector('#group_password').value = '';

    redraw(ctx, WIDTH, HEIGHT);
}

