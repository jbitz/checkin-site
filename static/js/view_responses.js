let code = '';
let auth = '';
let responses = [];

let graphData = {
    xNegLabel: "Tired",
    xPosLabel: "Energetic",
    yNegLabel: "Negative",
    yPosLabel: "Positive"
}

function setup() {
    let canvas = document.getElementById("main_canvas");
    let ctx = canvas.getContext("2d");

    let WIDTH = canvas.width;
    let HEIGHT = canvas.height;

    code = document.getElementById("parameters").dataset.code;
    auth = document.getElementById("parameters").dataset.auth;
    

    //Fetch the graph data
    const url = makePath(`/api/getresponses?code=${code}&auth=${auth}`);
    function update() {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                //TODO: Add something to do if data is empty (no survey)
                graphData = {
                    xNegLabel: data.xNegLabel,
                    xPosLabel: data.xPosLabel,
                    yNegLabel: data.yNegLabel,
                    yPosLabel: data.yPosLabel
                }

                responses = data.responses;
                redraw(ctx, WIDTH, HEIGHT);
            });
    }
    update();
    setInterval(update, 5000);

    if (document.getElementById('copy_button')) {
        document.getElementById('copy_button').innerText = "Copy Link";
    }
}

function redraw(ctx, WIDTH, HEIGHT) {
    //Clear off the Canvas
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
    drawAxes(ctx, WIDTH, HEIGHT, graphData);

    //Clear out the list of comments
    let listElem = document.getElementById("comment_list");
    while (listElem.lastChild) {
        listElem.removeChild(listElem.lastChild);
    }

    for (const response of responses) {
        //Draw circle on graph
        circleData = {
            x: response.xPos,
            y: response.yPos,
            innerRadius: 7,
            outerRadius: 17,
            innerColor: response.color,
            outerColor: response.color + "77",
            dragging: false
        }
        drawCircle(circleData, ctx, WIDTH, HEIGHT);

        //Add response comments with names
        if (response.comment.trim() !== '') {
            if(response.respondentName) {
                const nameNode = document.createElement('li');
                nameNode.classList.add('response-name');
                nameNode.innerHTML = response.respondentName + ':';
                listElem.appendChild(nameNode);
            }
            const commentNode = document.createElement('li');
            commentNode.classList.add('response-item');
            commentNode.innerHTML = response.comment;
            listElem.appendChild(commentNode);
        }
    }
}

function copyLink() {
    let tA = document.createElement("textarea");
    tA.style.position = 'fixed';
    tA.style.top = 0;
    tA.style.left = 0;
    tA.style.width = '2em';
    tA.style.height = '2em';
    tA.style.padding = 0;
    tA.style.border = 'none';
    tA.style.outline = 'none';
    tA.style.boxShadow = 'none';
    tA.style.background = 'transparent';

    tA.value = document.getElementById('respond_link').innerText;
    document.body.appendChild(tA);
    tA.focus();
    tA.select();
    document.execCommand('copy');
    document.body.removeChild(tA);

    document.getElementById('copy_button').innerText = "Link Copied!"
}