function drawAxes(ctx, WIDTH, HEIGHT, graphData) {
    ctx.lineWidth = 3;
    ctx.strokeStyle = "rgb(0, 0, 0)";
    ctx.beginPath();
    ctx.moveTo(0, HEIGHT / 2);
    ctx.lineTo(WIDTH, HEIGHT / 2);
    ctx.moveTo(WIDTH / 2, 0);
    ctx.lineTo(WIDTH / 2, HEIGHT);
    ctx.stroke();

    ctx.font = '20px serif';
    ctx.fillStyle = "rgb(0,0,0)";
    let MARGIN = 7;

    let measure = ctx.measureText("A"); //Make sure to use a tall letter
    ctx.fillText(graphData.yPosLabel, WIDTH / 2 + MARGIN, MARGIN + measure.actualBoundingBoxAscent);
    
    ctx.fillText(graphData.yNegLabel, WIDTH / 2 + MARGIN, HEIGHT - MARGIN);
    ctx.fillText(graphData.xNegLabel, MARGIN, HEIGHT / 2 - MARGIN);

    measure = ctx.measureText(graphData.xPosLabel);
    ctx.fillText(graphData.xPosLabel, WIDTH - MARGIN - measure.width, HEIGHT / 2 - MARGIN);
}

function drawCircle(circle, ctx, WIDTH, HEIGHT) {
    //Circle coordinates range from -128 to positive 128
    let coords = logicalToScreenCoords(circle, WIDTH, HEIGHT);
    ctx.fillStyle = circle.outerColor;
    ctx.beginPath();
    ctx.ellipse(coords.x, coords.y, circle.outerRadius, circle.outerRadius,
        0, 0, Math.PI * 2);
    ctx.fill();
    ctx.fillStyle = circle.innerColor;
    ctx.beginPath();
    ctx.ellipse(coords.x, coords.y, circle.innerRadius, circle.innerRadius,
        0, 0, Math.PI * 2);
    ctx.fill();
}

function getRelativeMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}

function logicalToScreenCoords(coords, WIDTH, HEIGHT) {
    let x = coords.x * (WIDTH / 256.0) + WIDTH / 2;
    let y = coords.y * (HEIGHT / 256.0) + HEIGHT / 2;
    return { x: x, y: y };
}

function screenToLogicalCoords(coords, WIDTH, HEIGHT) {
    let x = coords.x * (256.0 / WIDTH) - 128;
    let y = coords.y * (256.0 / HEIGHT) - 128;
    return { x: x, y: y };
}