surveys = [];
let canvas;
let ctx;

let WIDTH;
let HEIGHT;
let curSurveyIdx = 0;

function setup() {
    canvas = document.getElementById("main_canvas");
    ctx = canvas.getContext("2d");
    WIDTH = canvas.width;
    HEIGHT = canvas.height;

    infoDivs = document.querySelectorAll('.survey-info')
    for (let i = 0; i < infoDivs.length; i++) {
        const code = infoDivs[i].dataset.code;
        const auth = infoDivs[i].dataset.auth;
        const url = makePath(`/api/getresponses?code=${code}&auth=${auth}`);
        fetch(url)
            .then(response => response.json())
            .then(data => {
                // Store the data
                survey = {
                    graphData: {
                        xNegLabel: data.xNegLabel,
                        xPosLabel: data.xPosLabel,
                        yNegLabel: data.yNegLabel,
                        yPosLabel: data.yPosLabel
                    },
                    responses: data.responses,
                    timeCreated: new Date(data.timeCreated),
                };
                console.log(survey.timeCreated.getDate());
                surveys[i] = survey;
                showSurvey(survey);
                refreshInfo();
            });
    }

    document.querySelector('#previous_button').addEventListener('click', () => {
        curSurveyIdx = Math.max(0, curSurveyIdx - 1)
        refreshInfo();
    });

    document.querySelector('#next_button').addEventListener('click', () => {
        curSurveyIdx = Math.min(surveys.length - 1, curSurveyIdx + 1)
        refreshInfo();
    });
}

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
function refreshInfo() {
    const day = surveys[curSurveyIdx].timeCreated.getDay();
    const date = surveys[curSurveyIdx].timeCreated.getDate();
    const month = surveys[curSurveyIdx].timeCreated.getMonth() + 1; //Months start at 0
    const year = surveys[curSurveyIdx].timeCreated.getFullYear(); 
    document.querySelector('#date_text').innerText = `${DAYS[day]} ${month}/${date}/${year}`
    document.querySelector('#pager_text').innerText = `Survey ${curSurveyIdx + 1} / ${surveys.length}`;

    showSurvey(surveys[curSurveyIdx]);
}

function showSurvey(survey) {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
    drawAxes(ctx, WIDTH, HEIGHT, survey.graphData);

    //Clear out the list of comments
    let listElem = document.getElementById("comment_list");
    while (listElem.lastChild) {
        listElem.removeChild(listElem.lastChild);
    }

    for (const response of survey.responses) {
        //Draw circle on graph
        circleData = {
            x: response.xPos,
            y: response.yPos,
            innerRadius: 7,
            outerRadius: 17,
            innerColor: response.color,
            outerColor: response.color + "77",
            dragging: false
        }
        drawCircle(circleData, ctx, WIDTH, HEIGHT);

        //Add response comments with names
        if (response.comment.trim() !== '') {
            if (response.respondentName) {
                const nameNode = document.createElement('li');
                nameNode.classList.add('response-name');
                nameNode.innerHTML = response.respondentName + ':';
                listElem.appendChild(nameNode);
            }
            const commentNode = document.createElement('li');
            commentNode.classList.add('response-item');
            commentNode.innerHTML = response.comment;
            listElem.appendChild(commentNode);
        }
    }
}