let draggableCircle = {
    x: 0,
    y: 0,
    innerRadius: 5,
    outerRadius: 17,
    innerColor: "#ff0000",
    outerColor: "#ff000077",
    dragging: false
}

let graphData = {
    xNegLabel: "Tired",
    xPosLabel: "Energetic",
    yNegLabel: "Negative",
    yPosLabel: "Positive"
}

function send_data() {
    var comment_val = document.getElementById("comment").value;
    var x = Math.floor(draggableCircle.x);
    var y = Math.floor(draggableCircle.y);
    var code = document.getElementById("parameters").dataset.code;
    var request_body = { 
        comment: comment_val, 
        xPos: x, 
        yPos: y, 
        color: 
        draggableCircle.innerColor, 
        name: "",
        auth: document.getElementById("parameters").dataset.auth
    };

    let nameBox = document.getElementById("name");
    if(nameBox){
        if(nameBox.value.trim() === '') {
            document.getElementById('error_div').innerHTML = 'Please enter your name!';
            return;
        }
        request_body.name = nameBox.value;
    }

    fetch(makePath("/api/submitresponse?code=" + code), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(request_body)
    })
    .then((data) => {
        return data.json()
    })
    .then((data) => {
        if(data.url) {
            window.location.href = data.url;
        }
    });
}

function setup() {
    let canvas = document.getElementById("main_canvas");
    let ctx = canvas.getContext("2d");

    let WIDTH = canvas.width;
    let HEIGHT = canvas.height;

    let data = document.getElementById("parameters")
    //Data will be null if we are on the create survey page, because the
    //User will provide these values via the textbox inputs
    //But on the actual respond page, we need to pull the labels from the data
    //Sent by the server
    if(data){
        data = data.dataset;
        graphData = {
            xNegLabel: data.xneg,
            xPosLabel: data.xpos,
            yNegLabel: data.yneg,
            yPosLabel: data.ypos
        }
        if(data.asknames === 'False'){
            nameFields = document.getElementById("name_elements");
            nameFields.remove();
        }
    }
    
    //Set up color picker
    let colorPicker = document.getElementById("color_picker");
    draggableCircle.innerColor = colorPicker.value;
    draggableCircle.outerColor = colorPicker.value + "77";

    colorPicker.addEventListener("change", (e) => {
        draggableCircle.innerColor = e.target.value;
        draggableCircle.outerColor = e.target.value + "77";
        redraw(ctx, WIDTH, HEIGHT);
    }, false);

    //Make it respond to mouse events
    canvas.addEventListener('mousedown', (e) => {
        let mousePos = getRelativeMousePos(canvas, e);

        if (!draggableCircle.dragging) {
            let circlePos = logicalToScreenCoords(draggableCircle, WIDTH, HEIGHT);
            let d2 = Math.pow((mousePos.x - circlePos.x), 2) +
                Math.pow((mousePos.y - circlePos.y), 2);
            if (d2 < draggableCircle.outerRadius * draggableCircle.outerRadius) {
                draggableCircle.dragging = true;
            }
        } 
    });

    canvas.addEventListener('mouseup', (e) => {
        draggableCircle.dragging = false;
    });

    canvas.addEventListener('mousemove', (e) => {
        let mousePos = screenToLogicalCoords(getRelativeMousePos(canvas, e), WIDTH, HEIGHT);
        if (draggableCircle.dragging) {
            draggableCircle.x = mousePos.x;
            draggableCircle.y = mousePos.y;
        }

        mousePos = getRelativeMousePos(canvas, e);
        let circlePos = logicalToScreenCoords(draggableCircle, WIDTH, HEIGHT);
        let d2 = Math.pow((mousePos.x - circlePos.x), 2) +
            Math.pow((mousePos.y - circlePos.y), 2);
        if (d2 < draggableCircle.outerRadius * draggableCircle.outerRadius) {
            draggableCircle.outerColor = colorPicker.value + "CC"; //Less transparent
        } else {
            draggableCircle.outerColor = colorPicker.value + "77"; //More transparent
        }
        
        redraw(ctx, WIDTH, HEIGHT);
    });

    redraw(ctx, WIDTH, HEIGHT);
}

function redraw(ctx, WIDTH, HEIGHT) {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);

    //Draw axes
    drawAxes(ctx, WIDTH, HEIGHT, graphData);

    //Create movable object
    drawCircle(draggableCircle, ctx, WIDTH, HEIGHT);
}