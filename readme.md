# Checkin
Checkin is a web app which allows users to create scatter plot based surveys. It was created for use during online lectures at Hackbright Academy.

<center>
<img src="doc/demo-image.png" alt="Example Image" width="800"/>
</center>

## Deployment
### Docker Compose
The easiest way to deploy the site is via `docker compose`. Here is a sample configuration:
```yaml
services:
    db:
        image: postgres
        volumes:
            - postgres-vol:/var/lib/postgresql/data
        hostname: postgres-server
        environment:
            - POSTGRES_DB=checkin
            - POSTGRES_USER=postgres
            - POSTGRES_PASSWORD=postgres
    server:
        image: registry.gitlab.com/jbitz/checkin-site:latest
        command: gunicorn --worker-tmp-dir /dev/shm --workers=2 -b 0.0.0.0:5000 --log-file=- server:app
        ports:
            - "5000:5000"
        environment:
            - POSTGRES_HOST=postgres-server
            - POSTGRES_USER=postgres
            - POSTGRES_PASSWORD=postgres
            - FLASK_SECRET_KEY='___CHANGE_ME___'
            - APP_SUBDIRECTORY=/
        depends_on:
            - db
volumes:
    postgres-vol:
```
Create a file called `docker-compose.yml`, place the above configuration within it, then run
```bash
docker compose up
```
The server will be available on `localhost:5000`.

### Standalone
You can also run the Flask app outside of a Docker container. It is recommended to run the app inside of a virtual environment for dependency management.
```bash
git clone https://gitlab.com/jbitz/checkin-site.git
cd checkin-site
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```
Next, make sure the necessary environment variables exist (replace the bracketed values with the relevant information from your deployment environment)
```bash
touch env.sh
echo export POSTGRES_DB=[HOSTNAME] >> env.sh
echo export POSTGRES_USER=[USERNAME] >> env.sh
echo export POSTGRES_PASSWORD=[PASSWORD] >> env.sh
```
Finally, start the server. It will be available on port 5000.
```bash
source env.sh
python3 server.py
```
## Development
This git repository includes a `docker-compose.yml` file for easy local development.
```bash
git clone https://gitlab.com/jbitz/checkin-site.git && cd checkin-site
docker compose up
```
Changes made to the code will automatically be detected by the server without the need for a restart.
