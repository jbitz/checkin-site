from flask import Flask, render_template, redirect, url_for, request, session
from werkzeug.exceptions import abort
from werkzeug.security import check_password_hash, generate_password_hash

import crud
import model
import os

import random

app = Flask(__name__)
app.secret_key = os.environ['FLASK_SECRET_KEY']
model.connect_to_db(app)

AUTH_CODE_CHARS = list(map(chr, range(ord('A'), ord('Z') + 1)))
AUTH_CODE_LENGTH = 6

@app.context_processor
def inject_app_subdir():
    """
    Inject the variable app_subdirectory into every template,
    so that fetch requests, etc. can point to the right URL even if
    we're hosted under a subdirectory (like on the Frodo server)
    """
    return dict(app_subdirectory=os.environ.get('APP_SUBDIRECTORY'))

@app.route('/')
def index():
    return redirect(url_for('new_survey_form'))

@app.route('/new', methods=("GET", "POST"))
def new_survey_form():
    return render_template('create.html')

@app.route('/viewresponses')
def view_responses():
    if 'code' not in request.args or not request.args['code'].isnumeric():
        abort(400)

    view_link = False
    if 'viewlink' in request.args and bool(request.args['viewlink']):
        view_link = True

    survey = crud.get_survey(int(request.args['code']))
    if survey is None:
        abort(404)

    if survey.auth_code != '' and survey.auth_code != request.args.get('auth'):
        return redirect(url_for('show_unauthorized'))
    
    return render_template('view_responses.html', 
        code=request.args['code'].upper(), 
        view_link=view_link, 
        auth=request.args.get('auth')
    )

@app.route('/api/groupexists/<group_name>')
def group_exists(group_name):
    group = model.SurveyGroup.query.get(group_name.lower())
    return {'result': group is not None}


@app.route('/api/createsurvey', methods=('POST',))
def create_survey():
    data = request.json
    if not checkHasProperties(data, ['xneg', 'xpos', 'yneg', 'ypos', 'askName', 'isPrivate', 'groupData']):
        abort(400)

    # if we requested to join a group, verify that it's valid
    if data['isPrivate']:
        if not checkHasProperties(data['groupData'], ['code', 'password']):
            abort(400)

        group = model.SurveyGroup.query.get(data['groupData']['code'].lower())
        if not group: #create the group if it doesn't exist
            group = crud.create_survey_group(
                code=data['groupData']['code'], 
                password_hash=generate_password_hash(data['groupData']['password'])
            )

        if check_password_hash(group.password_hash, data['groupData']['password']): #success - we match!
            survey = crud.create_survey(
                xneg = data['xneg'],
                xpos = data['xpos'],
                yneg = data['yneg'],
                ypos = data['ypos'],
                ask_names= data['askName'],
                private=True,
                survey_group=group,
                auth_code = generate_auth_code()
            )
            return {
                'result': 'ok', 
                'url': url_for('view_responses') + f'?code={survey.survey_id}&viewlink=true&auth={survey.auth_code}'
            }       
        else:
            abort(403) #password was wrong!
            
    else: #create a public survey
        survey = crud.create_survey(
            xneg = data['xneg'],
            xpos = data['xpos'],
            yneg = data['yneg'],
            ypos = data['ypos'],
            ask_names= data['askName'],
        )
        return {
            'result': 'ok', 
            'url': url_for('view_responses') + f'?code={survey.survey_id}&viewlink=true'
        }  

@app.route('/api/getresponses')
def get_responses():
    if 'code' not in request.args or not request.args['code'].isnumeric():
        abort(400)
    
    code = request.args['code'].upper()

    survey = crud.get_survey(int(code))
    if survey is None:
        return {'result': 'Survey not found'}

    if survey.survey_group is not None: # trying to access a private survey
        if request.args.get('auth') == survey.auth_code:
            return survey.as_dict()
        else:
            return {'result': 'Invalid auth code'}
    
    return survey.as_dict()

@app.route('/api/submitresponse', methods=['POST'])
def accept_response():
    if 'code' not in request.args or not request.args['code'].isnumeric():
        abort(400)

    code = int(request.args['code'].upper())
    response = request.get_json()
        
    survey = crud.get_survey(code)
    if survey is None:
        abort(400)

    if survey.auth_code != '' and survey.auth_code != response.get('auth'):
        return {'result': 'Invalid auth code!'}

    name = response['name'] if response['name'] != '' else None
    crud.create_response(
        comment=response['comment'],
        x_pos=int(response['xPos']),
        y_pos=int(response['yPos']),
        color=response['color'],
        name=name,
        survey=survey
    )
    session['color'] = response['color']
    auth_string = '&auth=' + request.get_json().get('auth') if survey.survey_group else ''

    return {
        'result': 'success', 
        'url': url_for('view_responses') + f'?code={survey.survey_id}{auth_string}'
    }

@app.route('/respond')
def respond():
    if 'code' not in request.args or not request.args['code'].isnumeric():
        abort(400)
    code = int(request.args['code'].upper())

    survey = model.Survey.query.get(code)

    if survey is None:
        abort(400)

    if survey.auth_code != '' and survey.auth_code != request.args.get('auth'):
        return redirect(url_for('show_unauthorized'))
    

    color = '#ff0000'
    if 'color' in session:
        color = session['color']
    survey = crud.get_survey(int(code))
   
    return render_template('respond.html', survey=survey, color=color, auth=request.args.get('auth'))  

@app.route('/view_history', methods=['GET', 'POST'])
def view_history():
    if request.method == 'GET':
        # Just show the login template
        return render_template('view_history_login.html')
    
    survey_group = model.SurveyGroup.query.get(request.form['group_code'].lower())
    if survey_group is None:
        return render_template('view_history_login.html', error="Group does not exist!")

    
    elif not check_password_hash(survey_group.password_hash, request.form['group_password']):
        return render_template('view_history_login.html', error="Invalid password!")

    surveys = [{'auth': survey.auth_code, 'id': survey.survey_id, 'time_created': survey.time_created} for survey in survey_group.surveys]
    surveys.sort(key=lambda survey: survey['time_created'])
    return render_template('view_history.html', surveys=surveys, group_name=survey_group.group_code) 

@app.route('/unauthorized')
def show_unauthorized():
    return render_template('unauthorized.html')

def checkHasProperties(test_dict, prop_list):
    for item in prop_list:
        if item not in test_dict:
            return False
    return True

# Generate a random six digit string
def generate_auth_code():
    return ''.join([random.choice(AUTH_CODE_CHARS) for _ in range(AUTH_CODE_LENGTH)])

if __name__ == '__main__':
    print("Initialized server")
    app.run(host='0.0.0.0', debug=(True if os.environ.get('FLASK_DEBUG') == 'True' else False))